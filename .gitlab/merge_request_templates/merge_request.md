# Summary

<!-- One-sentence-summary what this merge request does -->

# What does it solve?

<!-- Describe what the problem (or previous behavior), why it is changed and what is changed about it. -->

# How does it solve it?

<!-- How is the change implemented in the PR on a high level? -->

# How to review

<!-- Hints how to review: What to look at in focus and how to test -->

# References

<!-- Link noteworthy references, such as the Jira ticket -->

# MR Checks
By submitting the MR, you confirm that you

* [ ] went through the requirements before submitting the PR, ensuring that every aspect of the requirements was implemented
* [ ] for functionality changes, wrote meaningful tests for any new/changed functionality
* [ ] ran all tests to verify that nothing is broken
* [ ] tested the **latest commit** to this MR yourself and verified that it completely works as expected (including Strapi, the App, Storybook, ETL, any other relevant components)
* [ ] wrote a meaningful "how to review" description that allows a reviewer to take the required steps
* [ ] used this template 🙃
