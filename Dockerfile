FROM node:16
COPY . /app
WORKDIR /app
RUN yarn
EXPOSE 3333
EXPOSE 4200
CMD CONFIG_FILE=/app/config.yml yarn start
