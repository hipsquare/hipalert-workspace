# HipAlert

![HipAlert Logo](./logo.svg)

HipAlert is a simple, extensible monitoring and alerting tool.

## Getting started

To get started quickly, please see our [quick start guide](./docs/quickstart.md).

You can also run the entire stack in a Docker container, see the [docker guide](./docs/docker.md).

## What HipAlert does

HipAlert is a simple, extensible monitoring and alerting tool. It consists of three main
components:

- **Checks** perform a check that can either be successful or unsuccessful. For example you could
  check for files to exist or for HTTP URLs to be reachable.
- **Alerts** are used to inform users when a check fails. For example, you might have an alert that
  logs errors to STDOUT and another one that sends out an alert email.
- **Summary Writers** write a summary of all checks and alerts somewhere. For example, a summary writer might publish a JSON file containing a list of all checks and an overall success status. Summary writers are especially practical if you want to expose the check status to other systems, e.g. by sending it off via REST or exposing it in-memory for the HipAlert backend's HTTP server.

The [runner](libs/hipalert-lib/src/runner.ts) takes care of parsing the HipAlert configuration and
executing it:

1. It runs all checks,
2. if checks fail, it uses alerts to publish these failures,
3. it writes a summary using the summary writers.

Additionally, an interval can be defined in which this check/alert/summary cycle is repeated.
In the sample config file, that's every second.

## Built-In Checks and Alerts

HipAlert brings some checks and alerts out of the box and ready to use:

**Checks**:
- [Disk Free Check](./docs/checks/disk-free-check.md)
- [File Exists Check](./docs/checks/file-exists-check.md)
- [HipAlert Check](./docs/checks/hip-alert-check.md)
- [HTTP](./docs/checks/http-check.md)


**Alerts**:

- [Console Log Alert](./docs/alerts/console-log-alert.md)
- [Email Alert](./docs/alerts/email-alert.md)
- [Pushover Alert](./docs/alerts/pushover-alert.md)

## Extending and integrating HipAlert

If you want to create your own checks, alerts or summary writers or integrate HipAlert within another application, see our [integration and extension guide](./docs/integration-and-extension.md).

## Developing HipAlert

To develop HipAlert, clone this repository and see [the development guide](./docs/development.md).

## Dockerized Execution

You can run HipAlert in a Docker container. See [the Docker guide](./docs/docker.md).