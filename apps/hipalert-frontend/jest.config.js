module.exports = {
  displayName: 'hipalert-frontend',
  preset: '../../jest.preset.js',
  transform: {
     '^.+\\.[tj]sx?$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/apps/hipalert-frontend',
  coverageReporters: ["cobertura", "text"],
  reporters: [["jest-junit", { outputName: "junit-hipalert-frontend.xml" }], "default"]
};
