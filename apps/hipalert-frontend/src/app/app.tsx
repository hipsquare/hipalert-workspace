import React from 'react';

import { Dashboard, HistoryEdu, Menu } from '@mui/icons-material';
import {
  AppBar,
  Box,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from '@mui/material';
import { useState } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import styled from 'styled-components';
import { BackendSelector } from './backend-selector';
import { History } from './history';
import { Summary } from './summary';
import { WithSummaryContext } from './summary-context';

const StyledApp = styled.div`
  h1 {
    font-weight: 200;
  }
`;

const Content = styled.div`
  padding: 24px;
`;

const NavigationLink = styled(Link)`
  color: unset;
  text-decoration: unset;
`;

export function App() {
  const [drawerOpen, setDrawerOpen] = useState(false);

  return (
    <BrowserRouter>
      <StyledApp>
        <Drawer
          anchor="left"
          open={drawerOpen}
          onClose={() => setDrawerOpen(false)}
        >
          <Box sx={{ minWidth: '250px' }}>
            <List>
              <NavigationLink to="/" onClick={() => setDrawerOpen(false)}>
                <ListItem button>
                  <ListItemIcon>
                    <Dashboard />
                  </ListItemIcon>
                  <ListItemText>Summary</ListItemText>
                </ListItem>
              </NavigationLink>
              <NavigationLink
                to="/history"
                onClick={() => setDrawerOpen(false)}
              >
                <ListItem button>
                  <ListItemIcon>
                    <HistoryEdu />
                  </ListItemIcon>
                  <ListItemText>History</ListItemText>
                </ListItem>
              </NavigationLink>
            </List>
          </Box>
        </Drawer>
        <WithSummaryContext>
          <AppBar position="static">
            <Toolbar>
              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 0 }}
                onClick={() => setDrawerOpen(true)}
              >
                <Menu />
              </IconButton>
              <Typography variant="h6">HipAlert</Typography>
              <BackendSelector />
            </Toolbar>
          </AppBar>
          <Content>
            <Routes>
              <Route path="/" element={<Summary />} />
              <Route path="/history" element={<History />} />
            </Routes>
          </Content>
        </WithSummaryContext>
      </StyledApp>
    </BrowserRouter>
  );
}

export default App;
