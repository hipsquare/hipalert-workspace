import React from 'react';

import styled from 'styled-components';
import { InputBase } from '@mui/material';
import { useContext } from 'react';
import { SummaryContext } from './summary-context';

const StyledTextField = styled(InputBase)`
  background: #fff;
  border-radius: 4px;
  margin-left: 25px;
  width: 100%;
  padding: 4px 12px;
`;

export function BackendSelector(): JSX.Element {
  const { backendBaseUrl, setBackendBaseUrl } = useContext(SummaryContext);
  return (
    <StyledTextField
      placeholder="Backend URL"
      value={backendBaseUrl}
      onChange={(e) => setBackendBaseUrl(e.target.value)}
    />
  );
}
