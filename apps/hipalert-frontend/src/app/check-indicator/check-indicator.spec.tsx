import React from 'react';

import { render } from '@testing-library/react';

import CheckIndicator from './check-indicator';

describe('CheckIndicator', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <CheckIndicator
        checkResult={{
          success: true,
        }}
        checkConfig={{
          name: '',
          title: '',
          settings: {},
        }}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
