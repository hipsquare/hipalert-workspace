import React from 'react';

import styled from 'styled-components';
import { CheckResultWithConfig } from '@hipsquare/hipalert-model';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 12px;
`;

const CheckOuter = styled.div`
  border: black 1px solid;
  width: 25px;
  height: 25px;
  border-radius: 50%;
`;

const CheckInner = styled.div<{ success: boolean }>`
  --color-success: #67d757;
  --color-failure: #c00;
  width: 19px;
  height: 19px;
  border-radius: 50%;
  background: var(
    ${({ success }) => (success ? '--color-success' : '--color-failure')}
  );
  left: 3px;
  top: 3px;
  position: relative;
`;

export function CheckIndicator({
  checkResult: { success },
  checkConfig: { title },
}: CheckResultWithConfig) {
  return (
    <Container>
      <CheckOuter>
        <CheckInner success={success} />
      </CheckOuter>
      {title}
    </Container>
  );
}

export default CheckIndicator;
