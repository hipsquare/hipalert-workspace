import React from 'react';

import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from '@mui/material';
import { useContext } from 'react';
import { SummaryContext } from './summary-context';
import CheckIndicator from './check-indicator/check-indicator';

export function History(): JSX.Element {
  const { summaryHistory } = useContext(SummaryContext);
  return (
    <>
      <br />
      <br />
      <Typography variant="h3">History</Typography>
      <br />
      {summaryHistory.map((summary) => (
        <Accordion>
          <AccordionSummary>
            <CheckIndicator
              checkResult={{ success: summary.overallSuccess }}
              checkConfig={{
                title: new Date(summary.date).toLocaleString(),
                name: '',
                settings: {},
              }}
            />
          </AccordionSummary>

          <AccordionDetails>
            <Typography fontWeight="medium">Checks: </Typography>
            <br />

            {summary.checks.map((check) => (
              <>
                <CheckIndicator
                  checkResult={check.checkResult}
                  checkConfig={check.checkConfig}
                />
                <br />
              </>
            ))}
          </AccordionDetails>
        </Accordion>
      ))}
    </>
  );
}
