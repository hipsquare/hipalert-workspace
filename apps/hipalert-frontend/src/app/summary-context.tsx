import React from 'react';

import { createContext, useEffect, useState } from 'react';
import { Summary } from '@hipsquare/hipalert-model';
import { environment } from '../environments/environment';

export const SummaryContext = createContext<{
  summary: Summary;
  summaryHistory: Summary[];
  backendBaseUrl: string;
  setBackendBaseUrl: (url: string) => void;
  error: boolean;
}>({
  summary: {
    checks: [],
    overallSuccess: true,
    date: '',
  },
  summaryHistory: [],
  backendBaseUrl: '',
  setBackendBaseUrl: () => {
    // empty on purpose
  },
  error: false,
});

export function WithSummaryContext({
  children,
}: {
  children: JSX.Element | JSX.Element[];
}): JSX.Element {
  const [summary, setSummary] = useState<Summary>({
    checks: [],
    overallSuccess: true,
    date: '',
  });
  const [summaryHistory, setSummaryHistory] = useState<Summary[]>([]);

  const [backendBaseUrl, setBackendBaseUrl] = useState(
    localStorage.getItem('backendBaseUrl') ?? environment.backendBaseUrl
  );
  const [debouncedBackendBaseUrl, setDebouncedBackendBaseUrl] =
    useState(backendBaseUrl);
  const [fetchError, setFetchError] = useState(false);

  useEffect(() => {
    localStorage.setItem('backendBaseUrl', debouncedBackendBaseUrl);

    const fetchSummary = () =>
      fetch(`${debouncedBackendBaseUrl}/summary`)
        .then((r) => r.json())
        .then((summary) => {
          setSummary(summary);
          setFetchError(false);
        })
        .catch(() => {
          setSummary({
            checks: [],
            overallSuccess: false,
            date: '',
          });
          setFetchError(true);
        });

    const fetchSummaryHistory = () =>
      fetch(`${debouncedBackendBaseUrl}/summary/history`)
        .then((r) => r.json())
        .then((summaryHistory) => setSummaryHistory(summaryHistory))
        .catch(() => setSummaryHistory([]));

    fetchSummary();
    fetchSummaryHistory();

    const interval = setInterval(() => {
      fetchSummary();
    }, environment.refreshInterval);
    return () => clearInterval(interval);
  }, [debouncedBackendBaseUrl]);

  useEffect(() => {
    const timer = setTimeout(
      () => setDebouncedBackendBaseUrl(backendBaseUrl),
      1000
    );
    return () => clearTimeout(timer);
  }, [backendBaseUrl]);

  return (
    <SummaryContext.Provider
      value={{
        summary,
        backendBaseUrl,
        setBackendBaseUrl,
        error: fetchError,
        summaryHistory,
      }}
    >
      {children}
    </SummaryContext.Provider>
  );
}
