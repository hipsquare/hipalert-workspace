import React from 'react';

import { useContext } from 'react';
import { CheckIndicator } from './check-indicator/check-indicator';
import { SummaryContext } from './summary-context';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Alert,
  Card,
  CardContent,
  Divider,
  Typography,
} from '@mui/material';
import styled from 'styled-components';

const ScrollableCardContent = styled(CardContent)`
  overflow-x: auto;
`;

export function Summary(): JSX.Element {
  const { summary, error } = useContext(SummaryContext);

  return !error ? (
    <>
      {summary.checks.map((check) => (
        <Accordion key={check.checkConfig.title}>
          <AccordionSummary>
            <CheckIndicator {...check} />
          </AccordionSummary>
          <AccordionDetails>
            {check.checkResult.message && (
              <>
                {!check.checkResult.success && (
                  <Alert severity="error">{check.checkResult.message}</Alert>
                )}
                {check.checkResult.success && (
                  <Alert severity="success">{check.checkResult.message}</Alert>
                )}
              </>
            )}
            {!check.checkResult.message && (
              <Alert severity="info">No message available for this alert</Alert>
            )}
            <Divider />
            <Card>
              <ScrollableCardContent>
                <Typography>
                  <code>
                    <pre>
                      {JSON.stringify(check.checkConfig.settings, null, 2)}
                    </pre>
                  </code>
                </Typography>
              </ScrollableCardContent>
            </Card>
          </AccordionDetails>
        </Accordion>
      ))}
    </>
  ) : (
    <>
      <h1>Summary</h1>
      <Alert severity="error">Could not fetch from given backend URL.</Alert>
    </>
  );
}
