export const environment = {
  production: true,
  backendBaseUrl: 'http://localhost:4444',
  refreshInterval: 5000,
};
