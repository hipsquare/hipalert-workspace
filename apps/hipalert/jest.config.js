module.exports = {
  displayName: 'hipalert',
  preset: '../../jest.preset.js',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageDirectory: '../../coverage/apps/hipalert',
  coverageReporters: ["cobertura", "text"],
  reporters: [["jest-junit", { outputName: "junit-hipalert.xml" }], "default"]
}
