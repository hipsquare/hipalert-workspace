# Console Log Alert

The Console Log Alert prints alerts on `STDERR` and resolution messages on `STDOUT`.

## Name to reference it in the configuration

`ConsoleLogAlert`

## Settings

This alert does not need any settings.

## Sample usage

```yaml

alerts:
  - name: ConsoleLogAlert
