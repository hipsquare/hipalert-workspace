# Email Alert

The email alert sends out alerts and resolution messages via email. It uses [Nodemailer](https://nodemailer.com/) under the hood.

## Name to reference it in the configuration

`EmailAlert`

## Settings

The alert accepts the following settings:

```typescript
{
  // the email server hostname
  host: string;

  // the email server port
  port: number;

  // the username for authentication
  username: string;

  // the password for authentication
  password: string;

  // whether or not to enable TLS
  secure: boolean;

  // the recipient address that alerts should be sent to
  to: string;

  // the sender address
  from: string;
}
```

## Sample usage

```yaml
alerts:
  - name: EmailAlert
    settings:
      host: mail.my-domain.com
      port: 25
      username: alerter
      password: pass
      secure: true
      to: me@my-domain.com
      from: alerter@my-domain.com
```
