# Pushover Alert

The Pushover alert sends alerts as push notifications using [Pushover](https://pushover.net/).

To get started, create a new application in Pushover. Use that application's API token for the alert settings. Also, copy the Pushover user key into the settings.

## Name to reference it in the configuration

`PushoverAlert`

## Settings

The alert accepts the following settings:

```typescript
{
  // the API token of the Pushover app you created
  apiToken: string;

  // your Pushover user key
  userKey: string;
}
```

## Sample usage

```yaml
alerts:
  - name: PushoverAlert
    settings:
      apiToken: abcdefghijklmnopqrstuvwyz
      userKey: zywvutsrqponmlkjihgfedcba
```
