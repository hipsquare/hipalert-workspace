# Disk Free Check

This check verifies that a minimum percentage of disk space is available on a mounted volume. It only works on Linux/Unix/MacOS, not on Windows, and uses the [df package](https://www.npmjs.com/package/df).

## Name to reference it in the configuration

`DiskFreeCheck`

## Settings

The alert accepts the following settings:

```typescript
{
  // mountpoint of the volume to check
  mountPoint: string;
  expectations: {
    // if the percentage of used vs free space is higher than this value, the check will be considered unsuccessful
    maxPercentUsed: number;
  }
}
```

## Sample usage

This check will fail if more than 80% of space are consumed on the disk mounted under `/`:

```yaml
checks:
  - name: DiskFreeCheck
    title: Disk / has space
    settings:
      mountPoint: /
      expectations:
        maxPercentUsed: 80
```
