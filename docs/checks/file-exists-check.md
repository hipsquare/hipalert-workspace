# File Exists Check

This check verifies that a file exists.

## Name to reference it in the configuration

`FileExistsCheck`

## Settings

The check accepts the following settings:

```typescript
{
  // the path of the file to check
  path: string;
}
```

## Sample usage

This check will fail if `/test-file.txt` does not exist:

```yaml
checks:
  - name: FileExistsCheck
    title: /test-file.txt exists
    settings:
      path: /test-file.txt
```
