# HipAlert Check

This check verifies that another HipSquare instance currently has no failing checks. It does so by using the `/summary` route of the HipAlert backend's HTTP Server. The check will fail if the remote HipSquare instance has at least one unsuccessful check.

## Name to reference it in the configuration

`HipAlertCheck`

## Settings

The alert accepts the following settings:

```typescript
{
  // the URL of the HipAlert instance's summary endpoint to check
  url: string;
}
```

## Sample usage

This check will fail if more than 80% of space are consumed on the disk mounted under `/`:

```yaml
checks:
  - name: HipAlertCheck
    title: Remote HipAlert runs successfully
    settings:
      url: http://my-other-hipalert-instance.com/summary
```
