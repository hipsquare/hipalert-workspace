# HTTP Check

This check checks a URL using HTTP(s) and can verify a set of expecations. 

In its basic shape, it checks that a valid HTTP response comes back (important note: that can mean any status code, including 400/500 error codes).

You can optionally add custom headers, define another HTTP method than GET, define a custom timeout (default is 5 seconds) and define custom expectations (expect a certain status code).

## Name to reference it in the configuration

`HipAlertCheck`

## Settings

The alert accepts the following settings:

```typescript
{
  // URL to check
  url: string;

  // HTTP method to use. Defaults to GET.
  method?: string;

  // optional headers to send the request with.
  headers?: Record<string, string>;

  /**
   * Timeout in milliseconds. Defaults to `5000`.
   */
  timeout?: number;

  expectations?: {
    /**
     * If set, the check will only succeed if the HTTP status code equals this status.
     */
    status: number;
  };
}
```

## Sample usage


```yaml
checks:
  - name: HttpCheck
    title: example.com is reachable 
    settings:
      url: https://example.com
      method: POST
      headers:
        my-header: my-value
      timeout: 10000
      expectations:
        status: 201
```
