# Developing HipAlert

The workspace consists of three projects:

- [HipAlert](./apps/hipalert): The backend that takes care of the main alert/check/summary writer
  cycle. It also exposes an HTTP endpoint `/summary` that contains a JSON summary of all checks and
  current alerts. It is configured using a `yaml` file that can be set with the environment
  variable `CONFIG_FILE`. For a sample config file, see [config.yml](./apps/hipalert/config.yml).
- The [HipAlert Frontend](./apps/hipalert-frontend): A frontend that reads the summary from the
  backend's `/summary` endpoint and renders it.
- The [HipAlert Data Model](./libs/hipalert-model-2): A TypeScript library that contains a common
  data model used by the backend and frontend.

## Quickstart from a locally cloned repository

To get everything running locally from the repository:

```shell
# Install dependencies
yarn

# Start the backend and frontend
yarn start:sample-config
```

Now open [the summary endpoint](http://localhost:3333/summary) to check out the current
summary, observe the STDOUT to see alerts popping up if anything changes and open 
[the frontend](http://localhost:4200) to see the overview.