# Run HipAlert in Docker

The entire HipAlert application (backend and frontend) is available in a Docker container as well.
See the [Dockerfile](./Dockerfile).

The `config.yml` file can be mounted under `/app/config.yml` to take effect. For a sample config file, check out [the quick start guide](./quickstart.md).

```shell
docker build -t hipalert .
docker -v /local/machine/path/to/config.yml:/app/config.yml \
    -p 3333:3333 \
    -p 4200:4200 \ 
    hipalert
```

You can now open [the frontend](http://localhost:4200) and [the backend summary endpoint](http://localhost:3333/summary).