# Integrating and Extending HipAlert

This guide will show you how to use the `@hipsquare/hipalert-lib` library in your own project and implement custom checks, alerts and summary writers.

## Create a project

```shell
mkdir project && cd project

# Install typescript
yarn add -D typescript ts-node-dev

# Basic typescript configuration
cat >> tsconfig.json << EOL
{
        "compilerOptions": {
                "target": "es2016",
                "module": "commonjs",
                "rootDir": "./src",
                "outDir": "./dist",
                "esModuleInterop": true,
                "forceConsistentCasingInFileNames": true,
                "strict": true,
                "skipLibCheck": true
        }
}
EOL
mkdir src

# install @hipsquare/hipalert-lib as a dependency
yarn add @hipsquare/hipalert-lib

```

## Set up the basic application

Create `src/main.ts`, which will be the entry point for the app:

```typescript
import { Runner } from '@hipsquare/hipalert-lib';

Runner.runFromConfigFile('config.yml');
```

The script uses the HipAlert `Runner` to start HipAlert from with the `config.yml` file.

Create a basic `config.yml`:

```yaml
checks:
alerts:
summaryWriters:

interval: 1000
```

Right now, the configuration does nothing, really, but you can start it nonetheless:

```shell
yarn ts-node-dev src/main.ts
```

## Build a Check

Continue by implementing a check. In this sample, it will check if a file is empty.

Create an empty file `src/FileEmptyCheck.ts`.

### Define settings

A check can be configured by settings that tell the check how to behave. For a file empty check, it certainly makes sense to make the path configurable under which the file to be checked resides.

Define the settings for the check:

```typescript
export interface FileEmptyCheckSettings {
  path: string;
}
```

### Implement the Check interface

Now create the check itself:

```typescript
import { Check } from '@hipsquare/hipalert-lib';

// ...

export class FileEmptyCheck implements Check<FileEmptyCheckSettings> {}
```

Checks implement the `Check` interface.

### getName()

The `getName` method needs to return the name of the check. This name is later used to identify which checks are referenced in the configuration:

```typescript
getName(): string {
    return 'FileEmptyCheck';
}
```

### setSettings()

The `setSettings` method is invoked to inject the settings defined in the HipAlert configuration of the given check:

```
private settings?: FileEmptyCheckSettings;

setSettings(settings: FileEmptyCheckSettings): void {
    this.settings = settings;
}
```

### check()

The `check` method is called to perform the actual check:

```typescript
import * as fs from "fs";
import { CheckResult } from "@hipsquare/hipalert-model";

// ...

async check(): Promise<CheckResult> {
    if (!this.settings?.path) {
        return {
            success: false,
            message: 'No path set in settings.'
        }
    }

    const fileContent = fs.readFileSync(this.settings.path).toString();

    if (fileContent === '') {
        return {
            success: true
        }
    }

    return {
        success: false,
        message: `File ${this.settings.path} is not empty.`
    };
}
```

It returns a promise that resolves to a `CheckResult`. The `CheckResult` has two properties: `success` is a boolean, indicating if the check ran successfully. `message` is an optional message that can be used to detail out errors in case of failure.

Please note that checks should always return a value and not throw exceptions to allow to properly register results.

### Result

The check now looks like this:

```typescript
import { Check } from '@hipsquare/hipalert-lib';
import * as fs from 'fs';
import { CheckResult } from '@hipsquare/hipalert-model';

export interface FileEmptyCheckSettings {
  path: string;
}

export class FileEmptyCheck implements Check<FileEmptyCheckSettings> {
  private settings?: FileEmptyCheckSettings;

  async check(): Promise<CheckResult> {
    if (!this.settings?.path) {
      return {
        success: false,
        message: 'No path set in settings.',
      };
    }

    if (!fs.existsSync(this.settings.path)) {
      return {
        success: false,
        message: `File ${this.settings.path} does not exist.`,
      };
    }

    const fileContent = fs.readFileSync(this.settings.path).toString();

    if (fileContent === '') {
      return {
        success: true,
      };
    }

    return {
      success: false,
      message: `File ${this.settings.path} is not empty.`,
    };
  }

  getName(): string {
    return 'FileEmptyCheck';
  }

  setSettings(settings: FileEmptyCheckSettings): void {
    this.settings = settings;
  }
}
```

### Register the check with HipAlert

The check is now functional, but in order for HipAlert to use it, it needs to be registered.

Add the registration call to `main.ts`:

```typescript
import { Runner } from '@hipsquare/hipalert-lib';
import { FileEmptyCheck } from './FileEmptyCheck';

Runner.registerCheck(new FileEmptyCheck());
Runner.runFromConfigFile('config.yml');
```

### Add the check to the configuration

Add the check to `config.yml`, plus a basic `console.log` alert to see messages:

```yaml
checks:
  - title: Is file empty?
    name: FileEmptyCheck
    settings:
      path: ./test.txt
alerts:
  - name: ConsoleLogAlert
summaryWriters:

interval: 1000
```

### Give it a spin

Start the application again and observe its behavior:

```shell
$ yarn ts-node-dev src/main.ts

Alert: Is file empty?: File ./test.txt does not exist.
```

Create `test.txt` and observe how the alert is resolved:

```shell
touch test.txt
```

Add some test to `test.txt` and observe how an alert is triggered again:

```shell
echo "non-empty" > test.txt
```

## Build an Alert

The same approach as for checks applies to alerts, too.

Alerts implement the `Alert` interface with a generic `Setting` parameter. They have settings injected using `setSettings` and return their name from `getName`.

### Define settings, implement basic methods

Create `src/SampleConsoleLogAlert.ts`:

```typescript
import { Alert, AlertPayload } from '@hipsquare/hipalert-lib';

export interface SampleConsoleLogAlertSettings {
  /**
   * If set to `true`, a current ISO timestamp will be printed in front of messages.
   */
  appendDateString: boolean;
}

export class SampleConsoleLogAlert
  implements Alert<SampleConsoleLogAlertSettings>
{
  settings?: SampleConsoleLogAlertSettings;

  getName(): string {
    return 'SampleConsoleLogAlert';
  }

  setSettings(settings: SampleConsoleLogAlertSettings): void {
    this.settings = settings;
  }
}
```

### alert() and resolved()

The `alert` and `resolved` methods are the core of what an alert does. When checks fail, all alerts that are configured are activated using the `alert` method. An array of `AlertPayload` objects is passed that contains one `AlertPayload` per failed check.

The same applies to `resolved`: After a check has succeeded again after failing previously, `resolved` will be called.

Implement these methods:

```typescript
async alert(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      console.log(`${this.getAppendedDate()}Alert for ${alert.checkTitle}: ${alert.message}`);
    }
  }

  resolved(alerts: AlertPayload[]) {
    for (const alert of alerts) {
        console.log(`${this.getAppendedDate()}Resolved for ${alert.checkTitle}: ${alert.message}`);
      }
  }

  private getAppendedDate(): string {
    if (!this.settings?.appendDateString) {
      return "";
    }
    return `[${new Date().toISOString()}] `;
  }
```

### Result

The resulting `src/SampleConsoleLogAlert.ts` file:

```typescript
import { Alert, AlertPayload } from '@hipsquare/hipalert-lib';

export interface SampleConsoleLogAlertSettings {
  /**
   * If set to `true`, a current ISO timestamp will be printed in front of messages.
   */
  appendDateString: boolean;
}

export class SampleConsoleLogAlert
  implements Alert<SampleConsoleLogAlertSettings>
{
  settings?: SampleConsoleLogAlertSettings;

  getName(): string {
    return 'SampleConsoleLogAlert';
  }

  setSettings(settings: SampleConsoleLogAlertSettings): void {
    this.settings = settings;
  }

  async alert(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      console.log(
        `${this.getAppendedDate()}Alert for ${alert.checkTitle}: ${
          alert.message
        }`
      );
    }
  }

  resolved(alerts: AlertPayload[]) {
    for (const alert of alerts) {
      console.log(
        `${this.getAppendedDate()}Resolved for ${alert.checkTitle}: ${
          alert.message
        }`
      );
    }
  }

  private getAppendedDate(): string {
    if (!this.settings?.appendDateString) {
      return '';
    }
    return `[${new Date().toISOString()}] `;
  }
}
```

### Register the alert

As for checks, alerts need to be registered with the runner. Add the registration to `main.ts`:

```typescript
import { Runner } from '@hipsquare/hipalert-lib';
import { FileEmptyCheck } from './FileEmptyCheck';
import { SampleConsoleLogAlert } from './SampleConsoleLogAlert';

Runner.registerCheck(new FileEmptyCheck());
Runner.registerAlert(new SampleConsoleLogAlert());
Runner.runFromConfigFile('config.yml');
```

### Configure the alert in the config file

```yaml
checks:
  - title: Is file empty?
    name: FileEmptyCheck
    settings:
      path: ./test.txt

alerts:
  - name: SampleConsoleLogAlert
    settings:
      appendDateString: true

summaryWriters:

interval: 1000
```

### Try it out

Run the application again and see how alerts are now reported using the new alert:

```shell
yarn ts-node-dev src/main.ts
```

## Build a summary writer

Summary writers receive a [`Summary` object](../libs/hipalert-model/src/lib/summary.ts) and write it to places (e.g. a file, a database, a web service).

The summary object contains a list of all checks with information on their success, plus an overall success status.

Summary writers follow the same logics as checks and alerts, so they have a generic `Settings` type and `getName` and `setSettings` methods. Additionally, they have a `write` method that is called when a new summary is available.

Write a sample summary writer that stores the summary in a JSON file:

```typescript
import * as fs from 'fs';
import { SummaryJsonWriter, SummaryWriter } from '@hipsquare/hipalert-lib';
import { Summary } from '@hipsquare/hipalert-model';

export interface SampleSummaryWriterSettings {
  path: string;
}

export class SampleSummaryWriter
  implements SummaryWriter<SampleSummaryWriterSettings>
{
  private settings?: SampleSummaryWriterSettings;

  getName(): string {
    return 'SimpleSummaryWriter';
  }

  setSettings(settings: SampleSummaryWriterSettings): void {
    this.settings = settings;
  }

  async write(summary: Summary): Promise<void> {
    if (!this.settings?.path) {
      return;
    }

    fs.writeFileSync(this.settings.path, JSON.stringify(summary));
  }
}
```

### Register the summary writer

As usually, you can let the HipAlert runner know about the summary writer by registering it. Add the registration to `main.ts`:

```typescript
import { Runner } from '@hipsquare/hipalert-lib';
import { FileEmptyCheck } from './FileEmptyCheck';
import { SampleConsoleLogAlert } from './SampleConsoleLogAlert';
import { SampleSummaryWriter } from './SampleSummaryWriter';

Runner.registerCheck(new FileEmptyCheck());
Runner.registerAlert(new SampleConsoleLogAlert());
Runner.registerSummaryWriter(new SampleSummaryWriter());
Runner.runFromConfigFile('config.yml');
```

### Add the summary writer to the configuration

Now update `config.yml` to include the summary writer:

```json
checks:
  - title: Is file empty?
    name: FileEmptyCheck
    settings:
      path: ./test.txt

alerts:
  - name: SampleConsoleLogAlert
    settings:
      appendDateString: true

summaryWriters:
  - name: SampleSummaryWriter
    settings:
      path: ./summary.json

interval: 1000
```

### Try out the summary writer

Run the application again:

```shell
yarn ts-node-dev src/main.ts
```

Check out how `summary.json` gets updated after every test run.

