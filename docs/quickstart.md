# Getting started with HipAlert

## Create a config file

Create a simple config in `config.yml`:

```yaml
checks:
  # Check the existence of `test.txt` 
  - name: FileExistsCheck
    title: Test file exists
    settings:
      path: ./test.txt

alerts:
  # Alert of broken checks via console.log messages
  - name: ConsoleLogAlert

interval: 10000
```

## Run HipAlert

```shell
CONFIG_FILE=./config.yml npx --package=@hipsquare/hipalert-lib start-hipalert
```

Using the `HOST` and `PORT` environment variables, you can make HipAlert listen to only a certain network interface and change the default port:

```shell
CONFIG_FILE=./config.yml HOST=127.0.0.1 PORT=3000 npx --package=@hipsquare/hipalert-lib start-hipalert
```

## Receive alerts

You should receive an alert in your console that `test.txt` doesn't exist:

```shell
$ CONFIG_FILE=./config.yml npx --package=@hipsquare/hipalert-lib start-hipalert
```

## Receive resolutions

Now when you `touch test.txt`, you should see a "resolved" message in the console:

```shell
$ CONFIG_FILE=./config.yml npx --package=@hipsquare/hipalert-lib start-hipalert

Alert: Test file exists: Path ./test.txt does not exist.
Resolved Test file exists: Test file exists resolved.
```

## Use the HTTP server

HipAlert also spins up an HTTP server to provide a summary of all checks and their results. You can retrieve it under [http://localhost:3333/summary](http://localhost:3333/summary).


## Use the HipAlert Frontend

HipAlert brings a small frontend application that shows the current status of all checks plus a check history. You can download [the latest version from the GitLab package registry](https://gitlab.com/hipsquare/hipalert-workspace/-/packages/5449133).

Unpack the archive and serve the frontend:

```shell
tar xvfj hipalert-latest.tar.bz2
cd hipalert-frontend
npx serve
```

Open [http://localhost:5000](http://localhost:5000) and set `http://localhost:3333` as the URL in the navigtion bar (if it isn't already set by default). You can now see your checks.
