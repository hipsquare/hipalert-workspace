# @hipsquare/hipalert-lib

This is the foundation for [HipAlert](https://gitlab.com/hipsquare/hipalert-workspace), a simple, Node-based monitoring and alerting solution. Please check out the [main repository](https://gitlab.com/hipsquare/hipalert-workspace) for information on how to use this package.