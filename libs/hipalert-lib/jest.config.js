module.exports = {
  displayName: 'hipalert-lib',
  preset: '../../jest.preset.js',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]sx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/libs/hipalert-lib',
  coverageReporters: ["cobertura", "text"],
  reporters: [["jest-junit", { outputName: "junit-hipalert-lib.xml" }], "default"]

};
