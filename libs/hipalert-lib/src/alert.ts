import { HasSettings } from './has-settings';

export interface AlertPayload {
  checkTitle: string;
  message: string;
}

export interface Alert<Settings> extends HasSettings<Settings> {
  getName(): string;
  alert(alerts: AlertPayload[]): Promise<void>;
  resolved(alerts: AlertPayload[]);
}
