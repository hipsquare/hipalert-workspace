import { AlertPayload } from '../../alert';
import { ConsoleLogAlert } from './console-log-alert';

describe('ConsoleLogAlert', () => {
  let alert: ConsoleLogAlert;
  let consoleLogSpy: jest.SpyInstance;
  let consoleErrorSpy: jest.SpyInstance;

  beforeEach(() => {
    alert = new ConsoleLogAlert();
    consoleLogSpy = jest.spyOn(console, 'log').mockImplementation(() => {
      // empty on purpose
    });
    consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation(() => {
      // empty on purpose
    });
  });

  it('should return a name', () => {
    expect(alert.getName()).toEqual('ConsoleLogAlert');
  });

  it('should log alerts to console', () => {
    // setup
    const alert1: AlertPayload = {
      message: 'Alert 1 Check Message',
      checkTitle: 'Alert 1 Check Title',
    };

    // test
    alert.alert([alert1]);

    // assert
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      expect.stringContaining(alert1.checkTitle)
    );
    expect(consoleErrorSpy).toHaveBeenCalledWith(
      expect.stringContaining(alert1.message)
    );
  });

  it('should log resolved messages to console', () => {
    // setup
    const alert1: AlertPayload = {
      message: 'Alert 1 Check Resolved Message',
      checkTitle: 'Alert 1 Check Title',
    };

    // test
    alert.resolved([alert1]);

    // assert
    expect(consoleLogSpy).toHaveBeenCalledWith(
      expect.stringContaining(alert1.checkTitle)
    );
    expect(consoleLogSpy).toHaveBeenCalledWith(
      expect.stringContaining(alert1.message)
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
