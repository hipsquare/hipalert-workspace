import { Alert, AlertPayload } from '../../alert';

/**
 * Alert that logs out to STDERR (for alerts) and STDOUT (for resolved alerts).
 */
export class ConsoleLogAlert implements Alert<void> {
  async alert(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      console.error(`Alert: ${alert.checkTitle}: ${alert.message}`);
    }
  }

  getName(): string {
    return 'ConsoleLogAlert';
  }

  resolved(alerts: AlertPayload[]) {
    for (const alert of alerts) {
      console.log(`Resolved ${alert.checkTitle}: ${alert.message}`);
    }
  }

  setSettings(): void {
    // empty on purpose
  }
}
