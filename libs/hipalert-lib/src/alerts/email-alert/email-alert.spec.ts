import { EmailAlert, EmailAlertSettings } from './email-alert';
import * as nodemailer from 'nodemailer';
import { Transporter } from 'nodemailer';
import { AlertPayload } from '../../alert';

describe('EmailAlert', () => {
  let alert: EmailAlert;
  const alertSettings: EmailAlertSettings = {
    host: 'host',
    port: 473,
    secure: false,
    username: 'user',
    password: 'pass',
    from: 'from@domain.com',
    to: 'to@domain.com',
  };
  let sendMailMock: jest.Mock;

  const alertPayload: AlertPayload = {
    message: 'Alert Message',
    checkTitle: 'Check Tittle',
  };

  beforeEach(() => {
    sendMailMock = jest.fn();
    jest.spyOn(nodemailer, 'createTransport').mockReturnValue({
      sendMail: sendMailMock,
    } as unknown as Transporter<unknown>);
    alert = new EmailAlert();
    alert.setSettings(alertSettings);
  });

  it('should return its name', () => {
    expect(alert.getName()).toEqual('EmailAlert');
  });

  it('should send out an alert via email', async () => {
    // test
    await alert.alert([alertPayload]);

    // assert
    expect(sendMailMock).toHaveBeenCalledWith({
      from: alertSettings.from,
      to: alertSettings.to,
      subject: expect.stringContaining(alertPayload.checkTitle),
      text: expect.stringContaining(alertPayload.message),
    });
  });

  it('should send out an resolved messages via email', async () => {
    // test
    await alert.resolved([alertPayload]);

    // assert
    expect(sendMailMock).toHaveBeenCalledWith({
      from: alertSettings.from,
      to: alertSettings.to,
      subject: expect.stringContaining(alertPayload.checkTitle),
      text: expect.stringContaining(alertPayload.message),
    });
  });
});
