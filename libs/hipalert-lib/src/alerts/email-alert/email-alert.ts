import { Alert, AlertPayload } from '../../alert';
import * as nodemailer from 'nodemailer';
import { Transporter } from 'nodemailer';

export interface EmailAlertSettings {
  host: string;
  port: number;
  username: string;
  password: string;
  secure: boolean;
  to: string;
  from: string;
}

/**
 * Alert that sends out emails.
 */
export class EmailAlert implements Alert<EmailAlertSettings> {
  private transport?: Transporter<unknown>;
  private settings?: EmailAlertSettings;

  async alert(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      await this.sendMessage(`⚠: ${alert.checkTitle} failed`, alert.message);
    }
  }

  async resolved(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      await this.sendMessage(`👌🏼: ${alert.checkTitle} resolved`, alert.message);
    }
  }

  private async sendMessage(subject: string, text: string): Promise<void> {
    if (!this.transport || !this.settings) {
      console.error(`EmailAlert could not send email: No settings defined.`);
      return;
    }

    try {
      await this.transport.sendMail({
        from: this.settings.from,
        to: this.settings.to,
        subject,
        text,
      });
    } catch (err) {
      console.error(
        `EmailAlert stumbled upon an error when trying to send an email: ${err}.`
      );
    }
  }

  getName(): string {
    return 'EmailAlert';
  }

  setSettings(settings: EmailAlertSettings): void {
    this.settings = settings;
    this.transport = nodemailer.createTransport({
      host: settings.host,
      port: settings.port,
      secure: settings.secure,
      auth: {
        user: settings.username,
        pass: settings.password,
      },
    });
  }
}
