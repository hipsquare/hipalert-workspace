import * as fetch from 'node-fetch';
import { PushoverAlert, PushoverAlertSettings } from './pushover-alert';
import { Response } from 'node-fetch';
import { AlertPayload } from '../../alert';

describe('PushoverAlert', () => {
  let alert: PushoverAlert;
  let fetchSpy: jest.Mocked<unknown>;
  const alertSettings: PushoverAlertSettings = {
    apiToken: 'apiToken',
    userKey: 'userKey',
  };

  const expectedHeaders = {
    method: 'POST',
  };

  beforeEach(() => {
    alert = new PushoverAlert();
    alert.setSettings(alertSettings);
    fetchSpy = jest.spyOn(fetch, 'default').mockResolvedValue({} as Response);
  });

  it('should return its name', () => {
    expect(alert.getName()).toEqual('PushoverAlert');
  });

  it('should send out messages when alerting', async () => {
    // setup
    const alertPayload: AlertPayload = {
      message: 'Check has failed',
      checkTitle: 'Check',
    };

    // test
    await alert.alert([alertPayload]);

    // assert
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining('https://api.pushover.net/1/messages.json'),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(`${alertPayload.checkTitle}`)),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(`${alertPayload.message}`)),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(alertSettings.apiToken)),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(alertSettings.userKey)),
      expectedHeaders
    );
  });

  it('should send out messages when resolving', async () => {
    // setup
    const alertPayload: AlertPayload = {
      message: 'Check has failed',
      checkTitle: 'Check',
    };

    // test
    await alert.resolved([alertPayload]);

    // assert
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining('https://api.pushover.net/1/messages.json'),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(
        encodeURIComponent(
          `${alertPayload.checkTitle} resolved: ${alertPayload.message}`
        )
      ),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(alertSettings.apiToken)),
      expectedHeaders
    );
    expect(fetchSpy).toHaveBeenCalledWith(
      expect.stringContaining(encodeURIComponent(alertSettings.userKey)),
      expectedHeaders
    );
  });
});
