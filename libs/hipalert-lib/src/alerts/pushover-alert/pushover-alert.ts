import fetch from 'node-fetch';
import { Alert, AlertPayload } from '../../alert';

export interface PushoverAlertSettings {
  apiToken: string;
  userKey: string;
}

/**
 * Alert that sends push notifications via [Pushover](https://pushover.net).
 */
export class PushoverAlert implements Alert<PushoverAlertSettings> {
  private settings?: PushoverAlertSettings;

  async alert(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      await this.sendMessage(`⚠: ${alert.checkTitle}: ${alert.message}`);
    }
  }

  async resolved(alerts: AlertPayload[]): Promise<void> {
    for (const alert of alerts) {
      await this.sendMessage(
        `👌: ${alert.checkTitle} resolved: ${alert.message}`
      );
    }
  }

  private async sendMessage(message: string): Promise<void> {
    if (!this.settings) {
      console.error('No settings provided for PushoverAlert.');
      return;
    }

    try {
      await fetch(
        `https://api.pushover.net/1/messages.json?message=${encodeURIComponent(
          message
        )}&token=${this.settings.apiToken}&user=${this.settings.userKey}`,
        {
          method: 'POST',
        }
      );
    } catch (error) {
      console.error(`Error sending notification via Pushover: ${error}`);
    }
  }

  getName(): string {
    return 'PushoverAlert';
  }

  setSettings(settings: PushoverAlertSettings): void {
    this.settings = settings;
  }
}
