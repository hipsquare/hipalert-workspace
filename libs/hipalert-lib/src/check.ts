import { HasSettings } from './has-settings';
import { CheckResult } from '@hipsquare/hipalert-model';

export interface Check<Settings> extends HasSettings<Settings> {
  check(): Promise<CheckResult>;
  getName(): string;
}
