import { DiskFreeCheck } from './disk-free-check';

describe('Disk Free Check', () => {
  let check: DiskFreeCheck;
  let dfImplementation: jest.Mock;

  beforeEach(() => {
    check = new DiskFreeCheck(dfImplementation);
    dfImplementation = jest
      .fn()
      .mockImplementation((callback) => callback(null, []));
  });

  it('should create', () => {
    expect(check).toBeTruthy();
  });

  it('should return its name', () => {
    expect(check.getName()).toEqual('DiskFreeCheck');
  });

  it('should fail if the mountpoint has not been found', async () => {
    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toContain('Could not find mount point');
  });

  it('should fail if the mount point has more space used than set in the settings', async () => {
    // setup
    const maxPercentUsed = 80;
    const mountPoint = '/';
    dfImplementation = jest.fn();
    dfImplementation.mockImplementation((callback) => {
      callback(null, [{ mountpoint: '/', percent: maxPercentUsed + 1 }]);
    });
    check = new DiskFreeCheck(dfImplementation);
    check.setSettings({
      mountPoint,
      expectations: {
        maxPercentUsed,
      },
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toContain('disk space used on mount point');
  });

  it('should succeed if the mount point has less space used than set in the settings', async () => {
    // setup
    const maxPercentUsed = 80;
    const mountPoint = '/';
    dfImplementation = jest.fn();
    dfImplementation.mockImplementation((callback) => {
      callback(null, [{ mountpoint: '/', percent: maxPercentUsed - 1 }]);
    });
    check = new DiskFreeCheck(dfImplementation);
    check.setSettings({
      mountPoint,
      expectations: {
        maxPercentUsed,
      },
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeTruthy();
  });
});
