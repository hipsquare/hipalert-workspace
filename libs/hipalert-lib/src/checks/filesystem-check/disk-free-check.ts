import { CheckResult } from '@hipsquare/hipalert-model';
import { Check } from '../../check';
import * as df from 'df';

export interface DiskFreeCheckSettings {
  mountPoint: string;
  expectations: {
    maxPercentUsed: number;
  };
}

interface DfEntry {
  mountpoint: string;
  percent: number;
}

export class DiskFreeCheck implements Check<DiskFreeCheckSettings> {
  private settings?: DiskFreeCheckSettings;

  constructor(
    private dfImplementation: (
      callback: (err: Error, dfEntries: DfEntry[]) => void
    ) => void = df
  ) {}

  check(): Promise<CheckResult> {
    const mountPoint = this.settings?.mountPoint ?? '/';
    const maxPercentUsedExpectation =
      this.settings?.expectations.maxPercentUsed ?? 90;

    return new Promise((resolve) => {
      this.dfImplementation((err, dfEntries: DfEntry[]) => {
        if (err) {
          return resolve({
            message: `Error trying to run df: ${err.toString()}`,
            success: false,
          });
        }

        const entryForMountpoint = dfEntries.find(
          (e) => e.mountpoint === mountPoint
        );
        if (!entryForMountpoint) {
          return resolve({
            message: `Could not find mount point ${mountPoint} in df table.`,
            success: false,
          });
        }

        if (entryForMountpoint.percent > maxPercentUsedExpectation) {
          return resolve({
            message: `${entryForMountpoint.percent}% disk space used on mount point "${mountPoint}". Expected: ${maxPercentUsedExpectation}%.`,
            success: false,
          });
        }

        resolve({ success: true });
      });
    });
  }

  getName(): string {
    return 'DiskFreeCheck';
  }

  setSettings(settings: DiskFreeCheckSettings): void {
    this.settings = settings;
  }
}
