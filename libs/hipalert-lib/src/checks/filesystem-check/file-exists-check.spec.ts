import * as fs from 'fs';
import { FileExistsCheck } from './file-exists-check';

describe('FileExistsCheck', () => {
  let check: FileExistsCheck;

  beforeEach(() => {
    check = new FileExistsCheck();
  });

  it('should return its name', () => {
    expect(check.getName()).toEqual('FileExistsCheck');
  });

  it('should run successfully if a file exits', async () => {
    // setup
    check.setSettings({
      path: '/',
    });
    jest.spyOn(fs, 'existsSync').mockReturnValue(true);

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeTruthy();
    expect(result.message).toBeUndefined();
  });

  it('should not run successfully if no settings were set', async () => {
    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toBeTruthy();
  });

  it('should not run successfully if the path to be checked does not exist', async () => {
    // setup
    check.setSettings({
      path: '/test',
    });
    jest.spyOn(fs, 'existsSync').mockReturnValue(false);

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toBeTruthy();
  });
});
