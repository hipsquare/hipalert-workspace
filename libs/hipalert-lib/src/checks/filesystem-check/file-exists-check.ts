import { Check } from '../../check';
import { CheckResult } from '@hipsquare/hipalert-model';
import * as fs from 'fs';

export interface FileExistsCheckSettings {
  path: string;
}

/**
 * Check that checks whether a file exists under a given path.
 */
export class FileExistsCheck implements Check<FileExistsCheckSettings> {
  private settings?: FileExistsCheckSettings;

  async check(): Promise<CheckResult> {
    if (!this.settings?.path) {
      return {
        success: false,
        message: 'No path defined to check.',
      };
    }

    if (!fs.existsSync(this.settings.path)) {
      return {
        success: false,
        message: `Path ${this.settings.path} does not exist.`,
      };
    }

    return {
      success: true,
    };
  }

  setSettings(settings: FileExistsCheckSettings): void {
    this.settings = settings;
  }

  getName(): string {
    return 'FileExistsCheck';
  }
}
