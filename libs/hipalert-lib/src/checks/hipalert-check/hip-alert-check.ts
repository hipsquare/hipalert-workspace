import { Check } from '../../check';
import { CheckResult, Summary } from '@hipsquare/hipalert-model';
import fetch from 'node-fetch';

export interface HipAlertCheckSettings {
  url: string;
}

/**
 * Check that checks the `overallSuccess` of another HipAlert instance.
 */
export class HipAlertCheck implements Check<HipAlertCheckSettings> {
  private settings?: HipAlertCheckSettings;

  async check(): Promise<CheckResult> {
    if (!this.settings?.url) {
      console.error(`HipAlert check failed: Could not find URL in settings.`);
      return;
    }
    try {
      const result: Summary = await fetch(this.settings.url).then((r) =>
        r.json()
      );
      return {
        success: result.overallSuccess,
        message: !result.overallSuccess
          ? `HipAlert under ${this.settings.url} failed.`
          : undefined,
      };
    } catch (err) {
      return {
        success: false,
        message: `Could not fetch HipAlert results from "${this.settings.url}": ${err}.`,
      };
    }
  }

  getName(): string {
    return 'HipAlertCheck';
  }

  setSettings(settings: HipAlertCheckSettings): void {
    this.settings = settings;
  }
}
