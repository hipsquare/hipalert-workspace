import * as fetch from 'node-fetch';
import { Response } from 'node-fetch';
import { HttpCheck } from './http-check';

describe('HttpCheck', () => {
  let check: HttpCheck;
  let fetchSpy: jest.Mocked<unknown>;

  beforeEach(() => {
    check = new HttpCheck();
    fetchSpy = jest.spyOn(fetch, 'default').mockResolvedValue({
      status: 200,
    } as Response);
  });

  it('should create', () => {
    expect(check).toBeTruthy();
  });

  it('should return its name', () => {
    expect(check.getName()).toEqual('HttpCheck');
  });

  it('should return false if no settings were given', async () => {
    // test
    const result = await check.check();

    // assert
    expect(result.success).toEqual(false);
    expect(result.message).not.toBeUndefined();
  });

  it('should return false if no URL was provided', async () => {
    // setup
    check.setSettings({
      url: '',
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toEqual(false);
    expect(result.message).not.toBeUndefined();
  });

  it('should call the given URL', async () => {
    // setup
    const url = 'https://www.my-url.com';
    check.setSettings({
      url,
    });

    // test
    await check.check();

    // assert
    expect(fetchSpy).toHaveBeenCalledWith(url, expect.anything());
  });

  it('should set headers from the config if some were provided', async () => {
    // setup
    const url = 'https://www.my-url.com';
    check.setSettings({
      url,
      headers: {
        a: 'b',
      },
    });

    // test
    await check.check();

    // assert
    expect(fetchSpy).toHaveBeenCalledWith(
      url,
      expect.objectContaining({
        headers: {
          a: 'b',
        },
      })
    );
  });

  it('should return true if the HTTP call succeeded', async () => {
    // setup
    const url = 'https://www.my-url.com';
    check.setSettings({
      url,
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeTruthy();
  });

  it('should return false if the HTTP call did not succeed for some reason', async () => {
    // setup
    const errorMessage = 'My error message';
    jest.spyOn(fetch, 'default').mockImplementation(() => {
      return Promise.reject({
        message: 'My error message',
      });
    });
    const url = 'https://www.my-url.com';
    check.setSettings({
      url,
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toContain(errorMessage);
  });

  it('should return false if an HTTP status is expected and another status was returned', async () => {
    // setup
    const url = 'https://www.my-url.com';
    check.setSettings({
      url,
      expectations: {
        status: 201,
      },
    });

    // test
    const result = await check.check();

    // assert
    expect(result.success).toBeFalsy();
    expect(result.message).toContain('201');
    expect(result.message).toContain('200');
  });

  it('should create a default timeout of 5,000ms', async () => {
    // setup
    const timeoutSpy = jest.spyOn(global, 'setTimeout');
    check.setSettings({
      url: 'https://www.test.com',
    });

    // test
    await check.check();

    // assert
    expect(timeoutSpy).toHaveBeenCalledWith(
      expect.anything(),
      HttpCheck.DEFAULT_TIMEOUT
    );
  });

  it('should a default timeout of 3,000ms by settings', async () => {
    // setup
    const timeout = 3_000;
    const timeoutSpy = jest.spyOn(global, 'setTimeout');
    check.setSettings({
      url: 'https://www.test.com',
      timeout,
    });

    // test
    await check.check();

    // assert
    expect(timeoutSpy).toHaveBeenCalledWith(expect.anything(), timeout);
  });
});
