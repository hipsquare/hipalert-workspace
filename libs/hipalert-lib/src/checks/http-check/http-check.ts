import { Check } from '../../check';
import { CheckResult } from '@hipsquare/hipalert-model';
import fetch from 'node-fetch';
import AbortController from 'abort-controller';

export interface HttpCheckSettings {
  url: string;
  method?: string;
  headers?: Record<string, string>;
  /**
   * Timeout in milliseconds. Defaults to `5000`.
   */
  timeout?: number;
  expectations?: {
    /**
     * If set, the check will only succeed if the HTTP status code equals this status.
     */
    status: number;
  };
}

/**
 * Check that checks a HTTP(S) URL.
 */
export class HttpCheck implements Check<HttpCheckSettings> {
  private settings?: HttpCheckSettings;
  static DEFAULT_TIMEOUT = 5_000;

  async check(): Promise<CheckResult> {
    if (!this.settings) {
      return {
        success: false,
        message: 'Settings not defined.',
      };
    }
    if (!this.settings.url) {
      return {
        success: false,
        message: 'No URL defined.',
      };
    }
    const abortController = new AbortController();

    try {
      setTimeout(
        () => abortController.abort(),
        this.settings.timeout ?? HttpCheck.DEFAULT_TIMEOUT
      );

      const result = await fetch(this.settings.url, {
        method: this.settings.method,
        headers: this.settings.headers,
        signal: abortController.signal,
      });

      const expectedStatus = this.settings.expectations?.status;
      if (expectedStatus != null && expectedStatus !== result.status) {
        return {
          success: false,
          message: `Received status ${result.status}, expected ${expectedStatus}`,
        };
      }

      return {
        success: true,
      };
    } catch (err) {
      console.log('Error: ', err);
      return {
        success: false,
        message: err.message ?? JSON.stringify(err) ?? 'Could not read error',
      };
    }
  }

  getName(): string {
    return 'HttpCheck';
  }

  setSettings(settings: HttpCheckSettings): void {
    this.settings = settings;
  }
}
