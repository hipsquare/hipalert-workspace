import { Runner } from '../runner';
import '../register';

const configPath = process.argv[2];

if (!configPath) {
  console.error('No config path given.');
  process.exit(1);
}

(async () => {
  await Runner.runFromConfigFile(configPath);
})();
