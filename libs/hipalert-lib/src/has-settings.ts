export interface HasSettings<Settings> {
  setSettings(settings: Settings): void;
}
