import cors = require('cors');
import express = require('express');
import { MemorySummaryWriter } from '../summary-writers';
import { Runner } from '../runner';

import '../register';

export function startHttpServer() {
  if (!process.env.CONFIG_FILE) {
    console.error(
      'No config file path provided in environment variable CONFIG_FILE'
    );
    process.exit(1);
  }

  Runner.runFromConfigFile(process.env.CONFIG_FILE);

  const app = express();
  app.use(cors());

  app.get('/summary', (req, res) => {
    res.send(MemorySummaryWriter.getSummary());
  });

  app.get('/summary/history', (req, res) => {
    res.send(MemorySummaryWriter.getSummaryHistory());
  });

  const port = process.env.PORT || 3333;
  const host = process.env.HOST || '0.0.0.0';

  const server = app.listen(+port, host, () => {
    console.log(`Listening at http://${host}:${port}/`);
  });
  server.on('error', console.error);
}
