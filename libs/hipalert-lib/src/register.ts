import { Runner } from './runner';
import { FileExistsCheck } from './checks';
import { HttpCheck } from './checks';
import { ConsoleLogAlert } from './alerts';
import { MemorySummaryWriter, SummaryJsonWriter } from './summary-writers';
import { PushoverAlert } from './alerts/pushover-alert/pushover-alert';
import { EmailAlert } from './alerts/email-alert/email-alert';
import { HipAlertCheck } from './checks/hipalert-check/hip-alert-check';
import { DiskFreeCheck } from './checks/filesystem-check/disk-free-check';

Runner.registerCheck(new DiskFreeCheck());
Runner.registerCheck(new FileExistsCheck());
Runner.registerCheck(new HttpCheck());
Runner.registerCheck(new HipAlertCheck());
Runner.registerAlert(new ConsoleLogAlert());
Runner.registerAlert(new EmailAlert());
Runner.registerAlert(new PushoverAlert());
Runner.registerSummaryWriter(new MemorySummaryWriter());
Runner.registerSummaryWriter(new SummaryJsonWriter());
