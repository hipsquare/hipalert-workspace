/**
 * The configuration of a HipAlert runner.
 */
import { CheckConfiguration } from '@hipsquare/hipalert-model';

export interface RunnerConfiguration {
  /**
   * The interval in which checks are performed (in miliseconds). If none is set, checks will only be performed once
   * on startup.
   */
  interval?: number;

  /**
   * Checks to be performed.
   */
  checks?: CheckConfiguration[];

  /**
   * Alerts to be used for reporting failed and recovered checks.
   */
  alerts?: {
    /**
     * Name of the alert (i.e. the result of `getName` of a class implementing `Alert`, usually the class name itself).
     */
    name: string;

    /**
     * Settings for the alert. The specific settings depend on the actual Alert class and can be found there as the
     * generic `Settings` attribute.
     */
    settings: unknown;
  }[];

  /**
   * Summary writers used for this configuration.
   */
  summaryWriters?: {
    /**
     * Name of the summary writer (i.e. the result of `getName` of a class implementing `SummaryWriter`, usually the
     * class name itself).
     */
    name: string;

    /**
     * Settings for the summary writer. The specific settings depend on the actual SummaryWriter class and can be found
     * there as the generic `Settings` attribute.
     */
    settings: unknown;
  }[];
}
