import { Runner } from './runner';
import { Check } from './check';
import { CheckResult } from '@hipsquare/hipalert-model';
import { Alert } from './alert';
import { SummaryWriter } from './summary-writer';

import './register';
import { RunnerConfiguration } from './runner-configuration';

class TruthyCheck implements Check<string> {
  async check(): Promise<CheckResult> {
    return {
      success: true,
    };
  }

  setSettings(): void {
    // empty on purpose
  }

  getName(): string {
    return 'TruthyCheck';
  }
}

class FalsyCheck implements Check<string> {
  async check(): Promise<CheckResult> {
    return {
      success: false,
      message: 'Falsy check is falsy',
    };
  }

  setSettings(): void {
    // empty on purpose
  }

  getName(): string {
    return 'FalsyCheck';
  }
}

class MockAlert implements Alert<string> {
  async alert(): Promise<void> {
    console.log('mock alert is alerting');
    // empty on purpose
  }

  async resolved(): Promise<void> {
    // empty on purpose
  }

  getName(): string {
    return 'MockAlert';
  }

  setSettings(): void {
    // empty on purpose
  }
}

class MockSummaryWriter implements SummaryWriter<void> {
  getName(): string {
    return 'MemorySummaryWriter';
  }

  setSettings(): void {
    // empty on purpose
  }

  write(): Promise<void> {
    return Promise.resolve(undefined);
  }
}

describe('Runner', () => {
  const truthyCheck = new TruthyCheck();
  const falsyCheck = new FalsyCheck();
  const mockChecks: Check<unknown>[] = [truthyCheck, falsyCheck];

  const mockAlert = new MockAlert();
  const mockAlerts: Alert<unknown>[] = [mockAlert];

  const mockSummaryWriter = new MockSummaryWriter();
  const mockSummaryWriters: SummaryWriter<unknown>[] = [mockSummaryWriter];

  let runner: Runner;

  const runnerConfig: RunnerConfiguration = {
    checks: [
      {
        name: 'TruthyCheck',
        title: 'Truthy Check',
        settings: {},
      },
    ],
    alerts: [
      {
        name: 'MockAlert',
        settings: {},
      },
    ],
    summaryWriters: [
      {
        name: 'MemorySummaryWriter',
        settings: {},
      },
    ],
  };

  beforeEach(() => {
    jest.useFakeTimers();

    runner = new Runner(mockChecks, mockAlerts, mockSummaryWriters);
    runner.setConfig(runnerConfig);
  });

  it('should call each check that is defined', async () => {
    // setup
    const checkCheckSpy = jest.spyOn(truthyCheck, 'check');
    const checkSetSettingsSpy = jest.spyOn(truthyCheck, 'setSettings');

    // test
    await runner.start();

    // assert
    expect(checkSetSettingsSpy).toHaveBeenCalledWith(
      runnerConfig.checks[0].settings
    );
    expect(checkCheckSpy).toHaveBeenCalled();
  });

  it('should not call any alerts if all checks are truthy', async () => {
    // setup
    const alertSpy = jest.spyOn(mockAlert, 'alert');

    // test
    await runner.start();

    // assert
    expect(alertSpy).not.toHaveBeenCalled();
  });

  it('should call the alerts and have the overall success false if at least one check came back as falsy', async () => {
    // setup
    const alertSpy = jest.spyOn(mockAlert, 'alert');
    const summarySpy = jest.spyOn(mockSummaryWriter, 'write');

    runner.setConfig({
      checks: [
        {
          name: 'TruthyCheck',
          title: 'Truthy Check',
          settings: {},
        },
        {
          name: 'FalsyCheck',
          title: 'Falsy Check',
          settings: {},
        },
      ],
      alerts: [
        {
          name: 'MockAlert',
          settings: {},
        },
      ],
      summaryWriters: runnerConfig.summaryWriters,
    });

    // test
    await runner.start();

    // assert
    expect(alertSpy).toHaveBeenCalled();
    expect(summarySpy).toHaveBeenCalledWith(
      expect.objectContaining({ overallSuccess: false })
    );
  });

  it('should write a summary with overallSuccess === true if no checks have failed', async () => {
    // setup
    const summarySpy = jest.spyOn(mockSummaryWriter, 'write');

    // test
    await runner.start();

    // assert
    expect(summarySpy).toHaveBeenCalledWith(
      expect.objectContaining({ overallSuccess: true })
    );
  });

  it('should run in an interval if the interval option is set', async () => {
    // setup
    const setIntervalSpy = jest.spyOn(global, 'setInterval');

    // test
    runner.setConfig({
      ...runnerConfig,
      interval: 1000,
    });
    await runner.start();

    // assert
    expect(setIntervalSpy).toHaveBeenCalledWith(expect.anything(), 1000);
    jest.useRealTimers();
  });

  it('should  alert on a failed check that is supposed to generate alerts after two cycles after only two failed cycles', async () => {
    // setup
    const interval = 1000;
    const config: RunnerConfiguration = {
      ...runnerConfig,
      checks: [
        {
          name: 'FalsyCheck',
          alertAfterNFailedCycles: 2,
          title: 'Falsy Check',
          settings: {},
        },
      ],
      interval,
    };
    const alertSpy = jest.spyOn(mockAlert, 'alert');
    runner.setConfig(config);

    // first run: fails, but should not generate an alert yet
    await runner.run();
    // second run: fails and should generate an alert
    await runner.run();
    // third run: fails, but as previous run already generated an alert, should not re-generate an alert
    await runner.run();
    // fourth run: fails, but as previous run already generated an alert, should not re-generate an alert
    await runner.run();

    expect(alertSpy).toHaveBeenCalledTimes(1);
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.useRealTimers();
  });
});
