import * as fs from 'fs';
import * as path from 'path';
import * as yaml from 'yaml';

import { Check } from './check';
import { Alert, AlertPayload } from './alert';
import { CheckResultWithConfig, Summary } from '@hipsquare/hipalert-model';
import { SummaryWriter } from './summary-writer';
import { RunnerConfiguration } from './runner-configuration';

interface CheckResultLog {
  time: number;
  checkResults: CheckResultWithConfig[];
}

export class Runner {
  private static CHECKS: Check<unknown>[] = [];
  private static ALERTS: Alert<unknown>[] = [];
  private static SUMMARY_WRITERS: SummaryWriter<unknown>[] = [];

  static registerCheck(check: Check<unknown>) {
    this.CHECKS.push(check);
  }

  static registerAlert(alert: Alert<unknown>) {
    this.ALERTS.push(alert);
  }

  static registerSummaryWriter(summaryWriter: SummaryWriter<unknown>) {
    this.SUMMARY_WRITERS.push(summaryWriter);
  }

  static async runFromConfigFile(configPath) {
    console.log('Reading configuration from', configPath);

    if (!fs.existsSync(path.resolve(configPath))) {
      console.error(`Path ${path.resolve(configPath)} does not exist.`);
      process.exit(1);
    }

    const runner = new Runner();

    const loadConfig = () => {
      try {
        const configText = fs.readFileSync(path.resolve(configPath)).toString();
        const config: RunnerConfiguration = yaml.parse(configText);
        runner.setConfig(config);
      } catch (err) {
        console.error(
          `Error parsing configuration file ${path.resolve(
            configPath
          )}: ${err}. Configuration remains unchanged.`
        );
      }
    };

    loadConfig();

    runner.setPreRunHook(loadConfig);
    await runner.start();
  }

  /**
   * Hook that will be run before every check/alert/summary cycle
   */
  private preRunHook?: () => unknown;

  private config: RunnerConfiguration = {
    checks: [],
    alerts: [],
    summaryWriters: [],
  };

  private intervalTimer: NodeJS.Timer;

  /**
   * Used to store check results after each run, and then to compare with in the next run to find checks that have
   * previously failed and are now recovered.
   */
  private checkResultLog: CheckResultLog[] = [];
  private checksTitlesUnderAlert: string[] = [];

  constructor(
    private checks: Check<unknown>[] = Runner.CHECKS,
    private alerts: Alert<unknown>[] = Runner.ALERTS,
    private summaryWriters: SummaryWriter<unknown>[] = Runner.SUMMARY_WRITERS
  ) {}

  setConfig(config: RunnerConfiguration) {
    this.config = {
      ...config,
      summaryWriters: [
        ...(config.summaryWriters ?? []),
        // memory summary writer should always be there
        {
          name: 'MemorySummaryWriter',
          settings: {},
        },
      ],
    };
  }

  setPreRunHook(hook: () => unknown) {
    this.preRunHook = hook;
  }

  /**
   * Starts the process in an interval if one is defined, or runs it a single time if not.
   */
  async start(): Promise<void> {
    if (!this.config.interval) {
      await this.run();
      return;
    }

    console.log('Running in interval of', this.config.interval, 'ms');
    await this.run();
    this.intervalTimer = setInterval(() => {
      this.run();
    }, this.config.interval);
  }

  async stop(): Promise<void> {
    if (this.intervalTimer) {
      clearInterval(this.intervalTimer);
    }
  }

  /**
   * Runs the checks one single time, generating a new summary and storing the results in `lastCheckResults`.
   */
  async run(): Promise<Summary> {
    this.preRunHook?.();

    const checkResults: CheckResultWithConfig[] = await this.runChecks();

    await this.triggerAlerts(checkResults);
    const summary = this.generateSummary(checkResults);
    await this.writeSummary(summary);

    this.checkResultLog.push({
      time: Date.now(),
      checkResults: checkResults,
    });

    return summary;
  }

  private async runChecks(): Promise<CheckResultWithConfig[]> {
    const checkResults: CheckResultWithConfig[] = [];

    for (const checkInConfig of this.config?.checks ?? []) {
      const check = this.getCheckWithName(checkInConfig.name);
      if (!check) {
        console.log('');
        console.warn(`Check named ${checkInConfig.name} not found.`);
        continue;
      }

      check.setSettings(checkInConfig.settings);
      const checkResult = await check.check();
      checkResults.push({
        checkResult,
        checkConfig: checkInConfig,
      });
    }

    return checkResults;
  }

  private getCheckWithName(name: string): Check<unknown> | undefined {
    return this.checks.find((check) => check.getName() === name);
  }

  private async triggerAlerts(
    checkResults: CheckResultWithConfig[]
  ): Promise<void> {
    const newAlerts = this.getNewAlerts(checkResults);
    const resolvedAlerts = this.getResolvedAlerts(checkResults);

    this.checksTitlesUnderAlert = [
      ...this.checksTitlesUnderAlert.filter(
        (checkTitle) =>
          !resolvedAlerts.map((a) => a.checkTitle).includes(checkTitle) &&
          !newAlerts.map((n) => n.checkTitle).includes(checkTitle)
      ),
      ...newAlerts.map((n) => n.checkTitle),
    ];

    for (const alertInConfig of this.config.alerts ?? []) {
      const alert = this.getAlertWithName(alertInConfig.name);

      if (!alert) {
        console.log('');
        console.warn(`Alert named ${alertInConfig.name} not found.`);
        continue;
      }

      alert.setSettings(alertInConfig.settings);

      if (newAlerts.length > 0) {
        await alert.alert(newAlerts);
      }
      if (resolvedAlerts.length > 0) {
        await alert.resolved(resolvedAlerts);
      }
    }
  }

  /**
   * Returns a list of alerts for checks that have newly failed.
   * @param currentCheckResults
   * @private
   */
  private getNewAlerts(
    currentCheckResults: CheckResultWithConfig[]
  ): AlertPayload[] {
    const checksToGenerateAlertsFor = currentCheckResults.filter((check) =>
      this.shouldAnAlertBeGeneratedForTheCheckInTheCurrentRun(check)
    );

    return checksToGenerateAlertsFor.map((checkResult) => ({
      message:
        checkResult.checkResult.message ??
        `Check ${checkResult.checkConfig.title} failed`,
      checkTitle: checkResult.checkConfig.title,
    }));
  }

  private getAlertWithName(name: string): Alert<unknown> | undefined {
    return this.alerts?.find((alert) => alert.getName() === name);
  }

  /**
   * Returns `true` if the given check should initiate an alert in the current run.
   */
  private shouldAnAlertBeGeneratedForTheCheckInTheCurrentRun(
    check: CheckResultWithConfig
  ): boolean {
    if (check.checkResult.success) {
      return false;
    }

    // if check is currently under alert, don't recreate an alert for it
    if (this.checksTitlesUnderAlert.includes(check.checkConfig.title)) {
      return false;
    }

    const considerCheckFailedAfterNCycles =
      check.checkConfig.alertAfterNFailedCycles ?? 1;

    if (
      this.checkResultLog.length === 0 &&
      considerCheckFailedAfterNCycles === 1
    ) {
      // first run ever, always alert then if something has failed for checks that have a failure cycle interval of 1 set
      return true;
    }

    // the last runs to consider for checks that require multiple cycles to have failed
    const relevantRunResults: CheckResultLog[] = this.checkResultLog.slice(
      -1 * considerCheckFailedAfterNCycles
    );

    // no sufficient number of runs is available to consider thi check failed
    if (relevantRunResults.length < considerCheckFailedAfterNCycles) {
      return false;
    }

    // for the relevant runs, identify whether the check was successful in any of them. if so, there was not the
    // required consecutive number of failures.
    return !relevantRunResults
      .map((run) =>
        run.checkResults.find(
          (cr) => cr.checkConfig.title === check.checkConfig.title
        )
      )
      .map(
        (checkResult: CheckResultWithConfig | undefined) =>
          checkResult?.checkResult.success ?? true
      )
      .includes(true);
  }

  /**
   * Returns alerts for checks that had failed in the last run, but are now successful, i.e. resolved alerts.
   */
  private getResolvedAlerts(
    currentCheckResults: CheckResultWithConfig[]
  ): AlertPayload[] {
    const resolvedChecks = currentCheckResults.filter(
      (result) =>
        result.checkResult.success &&
        this.checksTitlesUnderAlert.includes(result.checkConfig.title)
    );

    return (
      resolvedChecks.map((resolvedResult) => ({
        checkTitle: resolvedResult.checkConfig.title,
        message: `${resolvedResult.checkConfig.title} resolved.`,
      })) ?? []
    );
  }

  private generateSummary(checkResults: CheckResultWithConfig[]): Summary {
    return {
      checks: checkResults,
      overallSuccess:
        checkResults.filter((checkResult) => !checkResult.checkResult.success)
          .length === 0,
      date: new Date().toISOString(),
    };
  }

  private async writeSummary(summary: Summary) {
    for (const summaryWriterInConfig of this.config.summaryWriters ?? []) {
      const summaryWriter = this.getSummaryWriterWithName(
        summaryWriterInConfig.name
      );

      if (!summaryWriter) {
        console.log('');
        console.warn(
          `Summary writer called ${summaryWriterInConfig.name} not found.`
        );
        continue;
      }

      summaryWriter.setSettings(summaryWriterInConfig.settings);
      await summaryWriter.write(summary);
    }
  }

  private getSummaryWriterWithName(
    name: string
  ): SummaryWriter<unknown> | undefined {
    return this.summaryWriters?.find((sw) => sw.getName() === name);
  }

  private getLastCheckResults(): CheckResultWithConfig[] | undefined {
    return this.checkResultLog[this.checkResultLog.length - 1]?.checkResults;
  }
}
