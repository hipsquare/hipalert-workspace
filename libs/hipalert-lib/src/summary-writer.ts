import { Summary } from '@hipsquare/hipalert-model';
import { HasSettings } from './has-settings';

export interface SummaryWriter<Settings> extends HasSettings<Settings> {
  getName(): string;
  write(summary: Summary): Promise<void>;
}
