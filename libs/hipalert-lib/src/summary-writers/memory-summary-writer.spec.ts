import { Summary } from '@hipsquare/hipalert-model';
import { MemorySummaryWriter } from './memory-summary-writer';

describe('Memory Summary Writer', () => {
  let writer: MemorySummaryWriter;

  beforeEach(() => {
    writer = new MemorySummaryWriter();
  });

  it('should return its name', () => {
    expect(writer.getName()).toEqual('MemorySummaryWriter');
  });

  it('should write and retrieve the summary', async () => {
    // setup
    const summary: Summary = {
      checks: [],
      overallSuccess: true,
      date: '2022-01-02',
    };

    // test
    await writer.write(summary);

    // assert
    expect(MemorySummaryWriter.getSummary()).toEqual(summary);
  });
});
