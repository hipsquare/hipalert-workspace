import { SummaryWriter } from '../summary-writer';
import { Summary } from '@hipsquare/hipalert-model';

export interface MemorySummaryWriterSettings {
  /**
   * How many summaries to keep in memory. Defaults to 100.
   */
  maxSummaryItemsToKeep?: number;
}

/**
 * Keeps the summary in memory, available under the static method `getSummary`.
 */
export class MemorySummaryWriter
  implements SummaryWriter<MemorySummaryWriterSettings>
{
  private static summaryHistory: Summary[] = [];
  private static MAX_SUMMARY_ITEMS_TO_KEEP = 100;

  static getSummary(): Summary {
    return MemorySummaryWriter.summaryHistory[
      MemorySummaryWriter.summaryHistory.length - 1
    ];
  }

  static getSummaryHistory(): Summary[] {
    return MemorySummaryWriter.summaryHistory;
  }

  getName(): string {
    return 'MemorySummaryWriter';
  }

  setSettings({ maxSummaryItemsToKeep }: MemorySummaryWriterSettings): void {
    if (!maxSummaryItemsToKeep) {
      return;
    }
    MemorySummaryWriter.MAX_SUMMARY_ITEMS_TO_KEEP = maxSummaryItemsToKeep;
  }

  async write(summary: Summary): Promise<void> {
    if (
      MemorySummaryWriter.summaryHistory.length >=
      MemorySummaryWriter.MAX_SUMMARY_ITEMS_TO_KEEP
    ) {
      // cut off first n entries that are too much
      MemorySummaryWriter.summaryHistory.splice(
        0,
        MemorySummaryWriter.summaryHistory.length -
          MemorySummaryWriter.MAX_SUMMARY_ITEMS_TO_KEEP
      );
    }
    MemorySummaryWriter.summaryHistory.push(summary);
  }
}
