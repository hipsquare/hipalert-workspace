import { Summary } from '@hipsquare/hipalert-model';
import * as fs from 'fs';
import { SummaryJsonWriter } from './summary-json-writer';

describe('JSON Summary Writer', () => {
  let writer: SummaryJsonWriter;

  beforeEach(() => {
    writer = new SummaryJsonWriter();
  });

  it('should return its name', () => {
    expect(writer.getName()).toEqual('SummaryJsonWriter');
  });

  it('should write the summary to the configured path', async () => {
    // setup
    const summary: Summary = {
      checks: [],
      overallSuccess: true,
      date: '2021-02-02T00:01:02',
    };
    const path = 'my-path.json';
    const writeFileSpy = jest
      .spyOn(fs, 'writeFileSync')
      .mockImplementation(() => {
        // empty on purpose
      });

    writer.setSettings({
      path,
    });

    // test
    await writer.write(summary);

    // assert
    expect(writeFileSpy).toHaveBeenCalledWith(
      expect.stringContaining(path),
      JSON.stringify(summary, null, 2)
    );
  });
});
