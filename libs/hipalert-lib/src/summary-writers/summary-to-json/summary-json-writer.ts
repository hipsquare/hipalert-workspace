import { SummaryWriter } from '../../summary-writer';
import * as fs from 'fs';
import { Summary } from '@hipsquare/hipalert-model';

export interface SummaryJsonWriterSettings {
  path: string;
}

/**
 * Writes the summary to a JSON file.
 */
export class SummaryJsonWriter
  implements SummaryWriter<SummaryJsonWriterSettings>
{
  settings?: SummaryJsonWriterSettings;

  getName(): string {
    return 'SummaryJsonWriter';
  }

  setSettings(settings: SummaryJsonWriterSettings): void {
    this.settings = settings;
  }

  write(summary: Summary): Promise<void> {
    if (!this.settings?.path) {
      console.warn('No path given for SummaryJsonWriter.');
      return;
    }
    fs.writeFileSync(this.settings.path, JSON.stringify(summary, null, 2));
  }
}
