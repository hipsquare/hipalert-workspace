# @hipsquare/hipalert-model

This is the data model for [HipAlert](https://gitlab.com/hipsquare/hipalert-workspace), a simple, Node-based monitoring and alerting solution. Please check out the [main repository](https://gitlab.com/hipsquare/hipalert-workspace) for information on how to use this package.