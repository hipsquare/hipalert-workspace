export interface CheckConfiguration {
  /**
   * Name of the check (i.e. the result of `getName` of a class implementing `Check`, usually the class name itself).
   */
  name: string;

  /**
   * Human-readable title of the check. Will be used for summary and for sending alerts.
   */
  title: string;

  /**
   * Settings for the check. The specific settings depend on the actual Check class and can be found there as the
   * generic `Settings` attribute.
   */
  settings: unknown;

  /**
   * Alert that this check has failed only after this number of cycles.
   */
  alertAfterNFailedCycles?: number;
}
