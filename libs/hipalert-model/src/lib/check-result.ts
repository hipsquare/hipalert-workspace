import { CheckConfiguration } from './check-configuration';

export interface CheckResult {
  success: boolean;
  message?: string;
}

export interface CheckResultWithConfig {
  checkResult: CheckResult;
  checkConfig: CheckConfiguration;
}
