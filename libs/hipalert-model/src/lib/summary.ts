import { CheckResultWithConfig } from './check-result';

export interface Summary {
  /**
   * ISO Date String of when this summary was generated
   */
  date: string;
  overallSuccess: boolean;
  checks: CheckResultWithConfig[];
}
